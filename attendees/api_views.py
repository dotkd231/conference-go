import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from events.api_views import ConferenceDetailEncoder, ConferenceListEncoder
from events.models import Conference

from .models import Attendee


# def api_list_attendees(request, conference_id):
#     response = []
#     attendees = Attendee.objects.all()
#     for attendee in attendees:
#         response.append(
#             {
#                 "name": attendee.name,
#                 "href": attendee.get_api_url(),
#             }
#         )
#     return JsonResponse({"attendees": response})


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name",
        "email",
    ]


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_id):
    if request.method == "GET":
        attendees = Attendee.objects.filter(conference=conference_id)
        return JsonResponse(
            {"attendees": attendees}, encoder=AttendeeListEncoder, safe=False
        )
    else:
        content = json.loads(request.body)

        # Get the Conference object and put it in the content dict
        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

    attendee = Attendee.objects.create(**content)
    return JsonResponse(
        attendee,
        encoder=AttendeeDetailEncoder,
        safe=False,
    )


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        # # due to foreign key have to use encoders extra line
    ]
    encoders = {"conference": ConferenceListEncoder()}  # which one is correct?
    # def get_extra_data(self, o):
    #     return {"conference": o.conference.name}

    def get_extra_data(self, o):
        return {"conference": o.conference.name}


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_attendee(request, pk):
    if request.method == "GET":
        attendees = Attendee.objects.get(id=pk)
        return JsonResponse(
            attendees,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)

        # Get the Conference object and put it in the content dict
        try:
            conference = Conference.objects.get(id=ConferenceDetailEncoder)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

    attendee = Attendee.objects.create(**content)
    return JsonResponse(
        attendee,
        encoder=AttendeeDetailEncoder,
        safe=False,
    )

    # attendee = Attendee.objects.get(id=pk)
    # return JsonResponse(
    #     {
    #         "email": attendee.email,
    #         "name": attendee.name,
    #         "company_name": attendee.company_name,
    #         "created": attendee.created,
    #         "conference": {
    #             "name": attendee.name,
    #             "href": attendee.get_api_url(),
    #         },
    #     }
    # )
    # return JsonResponse(
    #     {"email", "name", "company_name", "created", "conference"}
