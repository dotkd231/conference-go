import json
from django.http import JsonResponse
from common.json import ModelEncoder
from events.acls import get_cord, get_photo, get_weather
from .models import Conference, Location, State
from django.views.decorators.http import require_http_methods


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "starts",
        "ends",
        "created",
        "updated",
        "max_presentations",
        "max_attendees",  # due to foreign key have to use encoders extra line
    ]


@require_http_methods(["DELETE", "GET", "PUT"])
def api_list_conferences(request):

    if request.method == "GET":
        response = []
        conferences = Conference.objects.all()
        for conference in conferences:
            response.append(
                {
                    "name": conference.name,
                    "href": conference.get_api_url(),
                }
            )
        return JsonResponse({"conferences": response})
    elif request.method == "DELETE":
        count, _ = Conference.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)

    # Get the Location object and put it in the content dict
    try:
        location = Location.objects.get(id=content["location"])
        content["location"] = location
    except Location.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid location id"},
            status=400,
        )

    conference = Conference.objects.create(**content)
    return JsonResponse(
        conference,
        encoder=ConferenceDetailEncoder,
        safe=False,
    )


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
    ]


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_conference(request, pk):
    if request.method == "GET":
        conference = Conference.objects.get(id=pk)
        coords = get_cord(
            conference.location.city, conference.location.state.abbreviation
        )
        weather = get_weather(coords["lat"], coords["lon"])
    return JsonResponse(
        {"conference": conference, "weather": weather},
        encoder=ConferenceDetailEncoder,
        safe=False,
    )
    # elif requiest.method == "DELETE":
    #     count, _= Conference.objects.filter(id=pk).delete()
    #     return JsonResponse({"deleted": count > 0})


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "image_url",
    ]

    def get_extra_data(self, o):
        return {"state": o.state.abbreviation}


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
    ]

    def get_extra_data(self, o):
        return {"state": o.state.abbreviation}


@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    if request.method == "GET":
        locations = Location.objects.all()
        return JsonResponse(
            {"locations": locations},
            encoder=LocationListEncoder,
        )
    else:
        content = json.loads(request.body)
        content["image_url"] = get_photo(content["city"], content["state"])
        # city = content["city"]
        # state = content["state"]
        # return res.json()["photos"][0]["src"]["original"]
        try:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )
        # Get the State object and put it in the content dict
        state = State.objects.get(abbreviation=content["state"])
        content["state"] = state

        location = Location.objects.create(**content)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )


# @require_http_methods
# def api_list_locations(request):
#     if request.method == "GET":
#         locations = Location.objects.all()
#         return JsonResponse(
#             {"locations": locations}, encoder=LocationListEncoder, safe=False
#         )
#     else:

#         content = json.loads(request.body)
#         content["image_url"] = get_photo(content["city"], content["state"])
#         res = requests.get(
#             "https://api.pexels.com/v1/search?query={city}+{state}&per_page1",
#             headers=headers,
#         )
#         print(res.json())
#         try:
#             state = State.objects.get(abbreviation=content["state"])
#             content["state"] = State
#         except State.DoesNotExist:
#             return JsonResponse(
#                 {"message": "Invalid state abbreviation"},
#                 status=400,
#             )


# def api_show_location(request, pk):
@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, pk):
    if request.method == "GET":
        location = Location.objects.get(id=pk)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})

    """
    Returns the details for the Location model specified
    by the pk parameter.

    This should return a dictionary with the name, city,
    room count, created, updated, and state abbreviation.

    {
        "name": location's name,
        "city": location's city,
        "room_count": the number of rooms available,
        "created": the date/time when the record was created,
        "updated": the date/time when the record was updated,
        "state": the two-letter abbreviation for the state,
    }
    """
    # location = Location.objects.get(id=pk)
    # return JsonResponse(
    #     {
    #         "name": location.name,
    #         "city": location.city,
    #         "room_count": location.room_count,
    #         "updated": location.updated,
    #         "state": {
    #             "name": location.state.abbreviation,
    #             "href": location.get_api_url(),
    #         },
    #     }
    # )


# def api_show_attendee(request, pk):
#     attendee = Attendee.objects.get(id=pk)
#     return JsonResponse(
#         attendee,
#         encoder=AttendeeDetailEncoder,
#         safe=False,
#     )
